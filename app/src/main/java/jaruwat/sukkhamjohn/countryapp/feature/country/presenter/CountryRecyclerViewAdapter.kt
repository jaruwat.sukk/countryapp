package jaruwat.sukkhamjohn.countryapp.feature.country.presenter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import jaruwat.sukkhamjohn.countryapp.R
import jaruwat.sukkhamjohn.countryapp.databinding.ItemCountryBinding
import jaruwat.sukkhamjohn.countryapp.databinding.ItemCountryEmptyBinding
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

class CountryRecyclerViewAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_EMPTY = 0
    private val TYPE_ITEM = 1

    var onClickItem: (country: Country) -> Unit = {}
    var onClickFavoriteItem: (country: Country) -> Unit = {}

    private var countryList = mutableListOf<Country>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            TYPE_EMPTY -> {
                val view = DataBindingUtil.inflate<ItemCountryEmptyBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_country_empty,
                    parent,
                    false
                )
                return CountryEmptyViewHolder(view)
            }
            else -> {
                val view = DataBindingUtil.inflate<ItemCountryBinding>(
                    LayoutInflater.from(parent.context),
                    R.layout.item_country,
                    parent,
                    false
                )
                return CountryViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (countryList.size > 0) {
            TYPE_ITEM
        } else {
            TYPE_EMPTY
        }
    }

    fun updateCountryList(newCountryList: List<Country>) {
        countryList.clear()
        countryList.addAll(newCountryList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = if (countryList.isNotEmpty()) countryList.size else 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CountryViewHolder -> {
                holder.apply {
                    bind(position)
                }
            }
        }
    }

    inner class CountryViewHolder(private val binding: ItemCountryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val country = countryList[position]
            binding.country = country

            binding.root.setOnClickListener {
                onClickItem.invoke(country)
            }

            binding.layoutFavorite.setOnClickListener {
                country.favorite = !country.favorite
                notifyItemChanged(position)
                onClickFavoriteItem.invoke(country)
            }

            binding.executePendingBindings()
        }
    }

    inner class CountryEmptyViewHolder(val binding: ItemCountryEmptyBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }
}
