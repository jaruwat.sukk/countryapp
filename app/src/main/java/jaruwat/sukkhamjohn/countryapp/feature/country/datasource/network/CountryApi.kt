package jaruwat.sukkhamjohn.countryapp.feature.country.datasource.network

import io.reactivex.Observable
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country
import retrofit2.http.GET

interface CountryApi {

    @GET("all")
    fun getAllCountry(): Observable<List<Country>>
}