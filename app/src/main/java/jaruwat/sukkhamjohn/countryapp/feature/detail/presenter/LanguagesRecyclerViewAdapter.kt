package jaruwat.sukkhamjohn.countryapp.feature.detail.presenter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import jaruwat.sukkhamjohn.countryapp.R
import jaruwat.sukkhamjohn.countryapp.databinding.ItemLanguageNameBinding
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Language

class LanguagesRecyclerViewAdapter :
    RecyclerView.Adapter<LanguagesRecyclerViewAdapter.LanguageViewHolder>() {
    private var languageList = mutableListOf<Language>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageViewHolder {
        val view = DataBindingUtil.inflate<ItemLanguageNameBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_language_name,
            parent,
            false
        )
        return LanguageViewHolder(view)
    }

    fun updateLanguageList(newLanguageList: List<Language>) {
        languageList.clear()
        languageList.addAll(newLanguageList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = languageList.size

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        holder.apply {
            bind(position)
        }
    }

    inner class LanguageViewHolder(private val binding: ItemLanguageNameBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val language = languageList[position]
            binding.number = position
            binding.language = language
            binding.executePendingBindings()
        }
    }
}
