package jaruwat.sukkhamjohn.countryapp.feature.detail.presenter

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import jaruwat.sukkhamjohn.countryapp.database.CountryRoomRepository
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

class CountryDetailPresenter(
    private val view: CountryDetailContract.View,
    private val countryDetailViewModel: CountryDetailViewModel,
    private val countryRoomRepository: CountryRoomRepository
) : CountryDetailContract.UserAction {

    private val compositeDisposable = CompositeDisposable()

    override fun handleStateCountryFavorite() {
        countryDetailViewModel.country.favorite = !countryDetailViewModel.country.favorite
        countryDetailViewModel.notifyChange()
        updateDataBase(country = countryDetailViewModel.country)
    }

    private fun updateDataBase(country: Country) {
        Observable.just(country)
            .subscribeOn(Schedulers.io())
            .subscribeBy {
                countryRoomRepository.updateCountry(it.name, it.favorite)
            }.addTo(compositeDisposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

}