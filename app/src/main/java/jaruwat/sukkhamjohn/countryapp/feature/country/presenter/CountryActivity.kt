package jaruwat.sukkhamjohn.countryapp.feature.country.presenter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import jaruwat.sukkhamjohn.countryapp.R
import jaruwat.sukkhamjohn.countryapp.databinding.ActivityCountryBinding
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country
import jaruwat.sukkhamjohn.countryapp.feature.detail.presenter.CountryDetailActivity
import jaruwat.sukkhamjohn.countryapp.feature.detail.presenter.CountryDetailActivity.Companion.COUNTRY_REQUEST_CODE
import kotlinx.android.synthetic.main.activity_country.*
import org.koin.androidx.scope.currentScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CountryActivity : AppCompatActivity(), CountryContract.View {

    private lateinit var countryRecyclerViewAdapter: CountryRecyclerViewAdapter
    private val countryViewModel: CountryViewModel by viewModel()

    private val presenter: CountryContract.UserAction by currentScope.inject {
        parametersOf(this, countryViewModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityCountryBinding>(this, R.layout.activity_country)
            .apply {
                this.viewModel = countryViewModel
            }

        initView()
        initEvent()
        presenter.getAllCountry()
    }

    override fun showShimmerView() {
        shimmerView.startShimmer()
    }

    override fun hideShimmerView() {
        shimmerView.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        shimmerView.stopShimmer()
    }

    override fun showErrorMessage(message: String) {
        Log.e("ERROR", message)
    }

    override fun showCountryList(countryList: List<Country>) {
        countryRecyclerViewAdapter.updateCountryList(countryList)
    }

    private fun initView() {
        countryRecyclerViewAdapter = CountryRecyclerViewAdapter()
    }

    private fun initEvent() {
        recyclerView.apply {
            adapter = countryRecyclerViewAdapter
        }

        countryRecyclerViewAdapter.onClickItem = { country ->
            CountryDetailActivity.navigate(this, country)
        }

        countryRecyclerViewAdapter.onClickFavoriteItem = { country ->
            presenter.updateDataBase(country)
            handleStateButtonAndList(country)
        }

        toggleButton.setOnCheckedChangeListener { _, isChecked ->
            countryViewModel.selectedFavorite = isChecked
            if (isChecked) {
                setCountryList(countryViewModel.countryList.filter { it.favorite })
            } else {
                setCountryList(countryViewModel.countryList)
            }
        }
    }

    private fun setCountryList(list: List<Country>) {
        countryViewModel.count = list.size
        countryViewModel.notifyChange()
        countryRecyclerViewAdapter.updateCountryList(list)
    }

    private fun handleStateButtonAndList(country: Country) {
        countryViewModel.countryList.first { it.name == country.name }.favorite =
            country.favorite
        if (countryViewModel.selectedFavorite) {
            setCountryList(countryViewModel.countryList.filter { it.favorite })
        } else {
            setCountryList(countryViewModel.countryList)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == COUNTRY_REQUEST_CODE) {
            val country =
                data?.getParcelableExtra<Country>(CountryDetailActivity::class.java.name)
            country?.let {
                handleStateButtonAndList(country)
            }
        }
    }
}
