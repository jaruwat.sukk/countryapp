package jaruwat.sukkhamjohn.countryapp.feature.country.presenter

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import jaruwat.sukkhamjohn.countryapp.base.UseCase
import jaruwat.sukkhamjohn.countryapp.database.CountryRoom
import jaruwat.sukkhamjohn.countryapp.database.CountryRoomRepository
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

class CountryPresenter(
    private val view: CountryContract.View,
    private val countryViewModel: CountryViewModel,
    private val countryRoomRepository: CountryRoomRepository,
    private val getAllCountryUseCase: UseCase<Unit, List<Country>>
) : CountryContract.UserAction {

    private val compositeDisposable = CompositeDisposable()

    override fun getAllCountry() {
        getAllCountryUseCase.execute(Unit)
            .map { countries ->
                if (countryRoomRepository.queryCountry().isNotEmpty()) {
                    Observable.fromIterable(countryRoomRepository.queryCountry())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy(
                            onNext = { roomData ->
                                countries.first { it.name == roomData.name }.favorite =
                                    roomData.favorite
                            },
                            onError = {
                                view.showErrorMessage(it.message ?: "")
                            }
                        )
                        .addTo(compositeDisposable)
                } else {
                    Observable.fromIterable(countries)
                        .subscribeOn(Schedulers.io())
                        .doFinally { view.hideShimmerView() }
                        .subscribeBy(
                            onNext = { country ->
                                countryRoomRepository.insertCountry(
                                    CountryRoom(
                                        name = country.name,
                                        favorite = country.favorite
                                    )
                                )
                            },
                            onError = {
                                view.showErrorMessage(it.message ?: "")

                            }
                        )
                        .addTo(compositeDisposable)
                }
                return@map countries
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view.showShimmerView() }
            .doFinally { view.hideShimmerView() }
            .subscribeBy(
                onNext = ::handleSuccessData,
                onError = {
                }
            )
            .addTo(compositeDisposable)
    }

    override fun updateDataBase(country: Country) {
        Observable.just(country)
            .subscribeOn(Schedulers.io())
            .subscribeBy {
                countryRoomRepository.updateCountry(it.name, it.favorite)
            }.addTo(compositeDisposable)
    }

    private fun handleSuccessData(response: List<Country>) {
        countryViewModel.countryList = response
        countryViewModel.count = response.size
        countryViewModel.notifyChange()
        view.showCountryList(countryViewModel.countryList)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}