package jaruwat.sukkhamjohn.countryapp.feature.country.presenter

import jaruwat.sukkhamjohn.countryapp.base.PresenterLifeCycleContract
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

interface CountryContract {

    interface View {
        fun showShimmerView()
        fun hideShimmerView()
        fun showErrorMessage(message: String)
        fun showCountryList(countryList: List<Country>)
    }

    interface UserAction :
        PresenterLifeCycleContract {
        fun getAllCountry()
        fun updateDataBase(country: Country)
    }
}