package jaruwat.sukkhamjohn.countryapp.feature.detail.presenter

import jaruwat.sukkhamjohn.countryapp.base.PresenterLifeCycleContract

interface CountryDetailContract {

    interface View {

    }

    interface UserAction :
        PresenterLifeCycleContract {
        fun handleStateCountryFavorite()
    }
}