package jaruwat.sukkhamjohn.countryapp.feature.country.domain

import io.reactivex.Observable
import jaruwat.sukkhamjohn.countryapp.base.UseCase
import jaruwat.sukkhamjohn.countryapp.feature.country.datasource.CountryDataSource
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

class GetAllCountryUseCase(private val countryRepository: CountryDataSource) :
    UseCase<Unit, List<Country>>() {

    override fun createObservable(request: Unit): Observable<List<Country>> =
        countryRepository.getAllCountry()
}