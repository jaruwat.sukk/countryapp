package jaruwat.sukkhamjohn.countryapp.feature.country.datasource

import io.reactivex.Observable
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

interface CountryDataSource {
    fun getAllCountry(): Observable<List<Country>>
}