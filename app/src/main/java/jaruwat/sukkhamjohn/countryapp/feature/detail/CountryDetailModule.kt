package jaruwat.sukkhamjohn.countryapp.feature.detail

import jaruwat.sukkhamjohn.countryapp.feature.detail.presenter.CountryDetailActivity
import jaruwat.sukkhamjohn.countryapp.feature.detail.presenter.CountryDetailContract
import jaruwat.sukkhamjohn.countryapp.feature.detail.presenter.CountryDetailPresenter
import jaruwat.sukkhamjohn.countryapp.feature.detail.presenter.CountryDetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val countryDetailModule = module {
    viewModel { CountryDetailViewModel() }

    scope(named<CountryDetailActivity>()) {
        scoped<CountryDetailContract.UserAction> { (view: CountryDetailContract.View, viewModel: CountryDetailViewModel) ->
            CountryDetailPresenter(
                view = view,
                countryDetailViewModel = viewModel,
                countryRoomRepository = get()
            )
        }
    }
}