package jaruwat.sukkhamjohn.countryapp.feature.country.presenter

import jaruwat.sukkhamjohn.countryapp.base.BaseViewModel
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

class CountryViewModel : BaseViewModel() {
    var countryList: List<Country> = listOf()
    var count: Int = 0
    var selectedFavorite = false
}