package jaruwat.sukkhamjohn.countryapp.feature.country

import jaruwat.sukkhamjohn.countryapp.network.NetworkClient
import jaruwat.sukkhamjohn.countryapp.feature.country.datasource.CountryDataSource
import jaruwat.sukkhamjohn.countryapp.feature.country.datasource.CountryRepository
import jaruwat.sukkhamjohn.countryapp.feature.country.datasource.network.CountryApi
import jaruwat.sukkhamjohn.countryapp.feature.country.datasource.network.CountryDataSourceNetwork
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.GetAllCountryUseCase
import jaruwat.sukkhamjohn.countryapp.database.CountryRoomRepository
import jaruwat.sukkhamjohn.countryapp.feature.country.presenter.CountryActivity
import jaruwat.sukkhamjohn.countryapp.feature.country.presenter.CountryContract
import jaruwat.sukkhamjohn.countryapp.feature.country.presenter.CountryPresenter
import jaruwat.sukkhamjohn.countryapp.feature.country.presenter.CountryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

private const val COUNTRY_USE_CASE = "COUNTRY_USE_CASE"

val countryModule = module {

    factory { NetworkClient().createApi<CountryApi>(get()) }
    factory<CountryDataSource> { CountryDataSourceNetwork(countryApi = get()) }
    factory { CountryRepository(remoteSourceCountry = get()) }
    factory(qualifier = named(COUNTRY_USE_CASE)) {
        GetAllCountryUseCase(countryRepository = get())
    }
    single { CountryRoomRepository(get()) }

    viewModel { CountryViewModel() }

    scope(named<CountryActivity>()) {
        scoped<CountryContract.UserAction> { (view: CountryContract.View, viewModel: CountryViewModel) ->
            CountryPresenter(
                view = view,
                countryViewModel = viewModel,
                getAllCountryUseCase = get(qualifier = named(COUNTRY_USE_CASE)),
                countryRoomRepository = get()
            )
        }
    }
}