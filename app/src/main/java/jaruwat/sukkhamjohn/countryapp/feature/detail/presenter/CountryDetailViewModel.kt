package jaruwat.sukkhamjohn.countryapp.feature.detail.presenter

import jaruwat.sukkhamjohn.countryapp.base.BaseViewModel
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

class CountryDetailViewModel : BaseViewModel() {
    var country: Country = Country()
}