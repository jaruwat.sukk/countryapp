package jaruwat.sukkhamjohn.countryapp.feature.country.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Country(
    @SerializedName("alpha2Code") val alpha2Code: String = "",
    @SerializedName("alpha3Code") val alpha3Code: String = "",
    @SerializedName("altSpellings") var altSpellings: List<String> = listOf(),
    @SerializedName("area") val area: Double = 0.0,
    @SerializedName("borders") var borders: List<String> = listOf(),
    @SerializedName("callingCodes") var callingCodes: List<String> = listOf(),
    @SerializedName("capital") val capital: String = "",
    @SerializedName("cioc") val cioc: String = "",
    @SerializedName("currencies") var currencies: List<Currency> = listOf(),
    @SerializedName("demonym") val demonym: String = "",
    @SerializedName("flag") val flag: String = "",
    @SerializedName("gini") val gini: Double = 0.0,
    @SerializedName("languages") var languages: List<Language> = listOf(),
    @SerializedName("latlng") var latlng: List<Double> = listOf(),
    @SerializedName("name") val name: String = "",
    @SerializedName("nativeName") val nativeName: String = "",
    @SerializedName("numericCode") val numericCode: String = "",
    @SerializedName("population") val population: Int = 0,
    @SerializedName("region") val region: String = "",
    @SerializedName("regionalBlocs") var regionalBlocs: List<RegionalBloc> = listOf(),
    @SerializedName("subregion") val subregion: String = "",
    @SerializedName("timezones") var timezones: List<String> = listOf(),
    @SerializedName("topLevelDomain") var topLevelDomain: List<String> = listOf(),
    @SerializedName("translations") var translations: Translations = Translations(),
    @SerializedName("favorite") var favorite: Boolean = false
) : Parcelable

