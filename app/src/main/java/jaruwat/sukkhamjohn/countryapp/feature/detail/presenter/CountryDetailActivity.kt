package jaruwat.sukkhamjohn.countryapp.feature.detail.presenter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import jaruwat.sukkhamjohn.countryapp.R
import jaruwat.sukkhamjohn.countryapp.database.CountryRoomRepository
import jaruwat.sukkhamjohn.countryapp.databinding.ActivityCountryDetailBinding
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country
import jaruwat.sukkhamjohn.countryapp.feature.country.presenter.CountryContract
import jaruwat.sukkhamjohn.countryapp.feature.country.presenter.CountryViewModel
import kotlinx.android.synthetic.main.activity_country_detail.*
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.currentScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CountryDetailActivity : AppCompatActivity(), CountryDetailContract.View {

    private lateinit var languageAdapter: LanguagesRecyclerViewAdapter
    private lateinit var callingCodeAdapter: CallingCodeRecyclerViewAdapter
    private lateinit var binding: ActivityCountryDetailBinding
    private val countryDetailViewModel: CountryDetailViewModel by viewModel()
    private var position: Int = 0

    private val presenter: CountryDetailContract.UserAction by currentScope.inject {
        parametersOf(this, countryDetailViewModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_country_detail)
        languageAdapter = LanguagesRecyclerViewAdapter()
        callingCodeAdapter = CallingCodeRecyclerViewAdapter()
        getDataFromIntent()

        languageRecyclerView.adapter = languageAdapter
        callingCodeRecyclerView.adapter = callingCodeAdapter

        callingCodeAdapter.onClickCallingCode = {
            navigateToDial(it)
        }

        layoutFavorite.setOnClickListener {
            presenter.handleStateCountryFavorite()
        }
    }

    override fun onBackPressed() {
        setIntentForActivityResult()
        super.onBackPressed()
    }

    private fun getDataFromIntent() {
        intent.getParcelableExtra<Country>(
            CountryDetailActivity::class.java.name
        ).let { data ->
            data?.let {
                countryDetailViewModel.country = it
                binding.viewModel = countryDetailViewModel
                languageAdapter.updateLanguageList(it.languages)
                callingCodeAdapter.updateCallingCodeList(it.callingCodes)
            }
        }
        position = intent.getIntExtra(POSITION, 0)
    }

    private fun navigateToDial(it: String) {
        startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", it, null)))
    }

    private fun setIntentForActivityResult() {
        val intent = intent
        intent.putExtra(CountryDetailActivity::class.java.name, countryDetailViewModel.country)
        this.setResult(Activity.RESULT_OK, intent)
    }

    companion object {
        const val COUNTRY_REQUEST_CODE = 1
        const val POSITION = "POSITION"
        fun navigate(activity: FragmentActivity, country: Country) {
            val intent = Intent(activity, CountryDetailActivity::class.java)
            intent.putExtra(CountryDetailActivity::class.java.name, country)
            activity.startActivityForResult(intent, COUNTRY_REQUEST_CODE)
        }
    }
}