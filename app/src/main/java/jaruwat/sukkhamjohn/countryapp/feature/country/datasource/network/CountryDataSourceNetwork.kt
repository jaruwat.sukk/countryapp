package jaruwat.sukkhamjohn.countryapp.feature.country.datasource.network

import io.reactivex.Observable
import jaruwat.sukkhamjohn.countryapp.feature.country.datasource.CountryDataSource
import jaruwat.sukkhamjohn.countryapp.feature.country.domain.model.Country

class CountryDataSourceNetwork(private val countryApi: CountryApi) :
    CountryDataSource {

    override fun getAllCountry(): Observable<List<Country>> {
        return countryApi.getAllCountry()
    }
}