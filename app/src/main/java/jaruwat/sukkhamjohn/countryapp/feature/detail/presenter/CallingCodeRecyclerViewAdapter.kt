package jaruwat.sukkhamjohn.countryapp.feature.detail.presenter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import jaruwat.sukkhamjohn.countryapp.R
import jaruwat.sukkhamjohn.countryapp.databinding.ItemCallingCodeBinding

class CallingCodeRecyclerViewAdapter :
    RecyclerView.Adapter<CallingCodeRecyclerViewAdapter.CallingCodeViewHolder>() {

    var onClickCallingCode: (String) -> Unit = { }
    private var callingCodeList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CallingCodeViewHolder {
        val view = DataBindingUtil.inflate<ItemCallingCodeBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_calling_code,
            parent,
            false
        )
        return CallingCodeViewHolder(view)
    }

    fun updateCallingCodeList(newCallingCode: List<String>) {
        callingCodeList.clear()
        callingCodeList.addAll(newCallingCode)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = callingCodeList.size

    override fun onBindViewHolder(holder: CallingCodeViewHolder, position: Int) {
        holder.apply {
            bind(position)
        }
    }

    inner class CallingCodeViewHolder(private val binding: ItemCallingCodeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val callingCode = callingCodeList[position]
            binding.number = position
            binding.callingCode = callingCode

            binding.root.setOnClickListener {
                onClickCallingCode.invoke(callingCode)
            }

            binding.executePendingBindings()
        }
    }
}
