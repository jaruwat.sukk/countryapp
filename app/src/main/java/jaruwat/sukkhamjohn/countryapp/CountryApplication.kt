package jaruwat.sukkhamjohn.countryapp

import android.app.Application
import jaruwat.sukkhamjohn.countryapp.base.networkModule
import jaruwat.sukkhamjohn.countryapp.feature.country.countryModule
import jaruwat.sukkhamjohn.countryapp.feature.detail.countryDetailModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CountryApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@CountryApplication)
            modules(
                listOf(
                    networkModule,
                    countryModule,
                    countryDetailModule
                )
            )
        }
    }
}