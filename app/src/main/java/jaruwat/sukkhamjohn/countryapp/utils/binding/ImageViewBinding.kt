package jaruwat.sukkhamjohn.countryapp.utils.binding

import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import jaruwat.sukkhamjohn.countryapp.utils.glide.SvgSoftwareLayerSetter

@BindingAdapter("imageFromSvgUrl")
fun bindImageFromSvgUrl(imageView: ImageView, url: String?) {
    val uri = Uri.parse(url)
    Glide.with(imageView.context)
        .`as`(PictureDrawable::class.java)
        .listener(SvgSoftwareLayerSetter())
        .load(uri)
        .into(imageView)
}