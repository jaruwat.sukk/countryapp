package jaruwat.sukkhamjohn.countryapp.base

import io.reactivex.Observable

abstract class UseCase<in REQUEST, RESPONSE> {

    internal abstract fun createObservable(request: REQUEST): Observable<RESPONSE>

    fun execute(request: REQUEST): Observable<RESPONSE> {
        return createObservable(request)
    }
}