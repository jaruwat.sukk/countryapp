package jaruwat.sukkhamjohn.countryapp.base

import jaruwat.sukkhamjohn.countryapp.network.OkHttpBuilder
import org.koin.dsl.module

val networkModule = module {
    single {
        OkHttpBuilder().build()
    }
}