package jaruwat.sukkhamjohn.countryapp.base

interface PresenterLifeCycleContract {
	fun onDestroy()
}