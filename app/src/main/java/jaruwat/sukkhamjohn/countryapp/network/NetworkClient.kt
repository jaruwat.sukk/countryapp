package jaruwat.sukkhamjohn.countryapp.network

import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import jaruwat.sukkhamjohn.countryapp.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NetworkClient {

    inline fun <reified ApiService> createApi(okHttpClient: OkHttpClient): ApiService {
        val gsonBuilder = GsonBuilder().setLenient()
        val gson = gsonBuilder.create()
        val gsonConverterFactory = GsonConverterFactory.create(gson)
        val rxJava2CallAdapterFactory =
            RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

        val retrofit = retrofit2.Retrofit.Builder()
            .baseUrl(BuildConfig.SERVICE_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .build()

        return retrofit.create(ApiService::class.java)
    }
}