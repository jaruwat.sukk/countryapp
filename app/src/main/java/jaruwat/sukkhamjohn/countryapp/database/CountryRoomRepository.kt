package jaruwat.sukkhamjohn.countryapp.database

import android.content.Context

class CountryRoomRepository(
    context: Context
) {
    private var countryDao: CountryDao? = null

    init {
        val roomDB = CountryDatabase.getInstance(context)
        countryDao = roomDB.countryDao()
    }

    fun insertCountry(countryRoom: CountryRoom) {
        countryDao?.insertAll(countryRoom)
    }

    fun updateCountry(name: String, favorite: Boolean) {
        countryDao?.updateCountry(name = name, favorite = favorite)
    }

    fun queryCountry(): List<CountryRoom> = countryDao?.getAll() ?: throw NullPointerException()

}