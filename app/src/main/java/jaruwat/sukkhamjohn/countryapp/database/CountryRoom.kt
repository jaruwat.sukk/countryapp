package jaruwat.sukkhamjohn.countryapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.jetbrains.annotations.NotNull

@Entity(tableName = "countryRoom")
data class CountryRoom(
    @PrimaryKey(autoGenerate = true)
    @NotNull
    var id: Int = 0,

    @ColumnInfo(name = "name") val name: String = "",
    @ColumnInfo(name = "favorite") var favorite: Boolean = false
)