package jaruwat.sukkhamjohn.countryapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import java.lang.NullPointerException

private const val DB_NAME: String = "coountry_db"

@Database(entities = arrayOf(CountryRoom::class), version = 1, exportSchema = true)
abstract class CountryDatabase : RoomDatabase() {
    abstract fun countryDao(): CountryDao

    companion object {
        private var instance: CountryDatabase? = null

        fun getInstance(context: Context): CountryDatabase {
            if (instance == null) {
                synchronized(CountryDatabase::class) {
                    //Create DataBase
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        CountryDatabase::class.java,
                        DB_NAME
                    )
                        .build()
                }
            }

            return instance
                ?: throw NullPointerException()
        }
    }
}