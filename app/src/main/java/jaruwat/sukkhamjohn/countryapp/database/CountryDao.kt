package jaruwat.sukkhamjohn.countryapp.database

import androidx.room.*

@Dao
interface CountryDao {
    @Query("SELECT * FROM countryRoom")
    fun getAll(): List<CountryRoom>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(countryRoom: CountryRoom)

    @Query("UPDATE countryRoom SET favorite =:favorite WHERE name = :name")
    fun updateCountry(name: String, favorite: Boolean)

    @Delete
    fun delete(countryRoom: CountryRoom)
}

